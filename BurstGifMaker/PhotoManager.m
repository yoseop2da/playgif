//
//  PhotoManager.m
//  ImageEditor
//
//  Created by parkyoseop on 2016. 5. 19..
//  Copyright © 2016년 Yoseop Park. All rights reserved.
//

#import "PhotoManager.h"
@interface PhotoManager()
{
    PHCachingImageManager *_imageManager;
}
@end

@implementation PhotoManager

+ (id)sharedInstance
{
    static dispatch_once_t p = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

- (void)checkPermission:(void(^)(BOOL isAuthorized))block
{
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status) {
            case PHAuthorizationStatusAuthorized:
                block(YES);
                break;
            case PHAuthorizationStatusRestricted:
                block(NO);
                break;
            case PHAuthorizationStatusDenied:
                block(NO);
                break;
            case PHAuthorizationStatusNotDetermined:
                block(NO);
                break;
            default:
                break;
        }
    }];
}

#pragma mark - 이미지 저장 및 삭제.

- (PHFetchResult<PHAsset *> *)getAllPhotos
{
//    PHFetchOptions *options = [[PHFetchOptions alloc] init];
//    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
//    
//    PHFetchResult *allPhotos = [PHAsset fetchAssetsWithOptions:options];

    PHFetchResult *allPhotos = [PHAsset fetchAssetsWithOptions:nil];
    return allPhotos;
}

- (PHFetchResult<PHAssetCollection *> *)getAlbums
{
    PHFetchResult *collections = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    return collections;
}

/*
 Favorites  /   Recently Deleted    /   Panoramas   /   Camera Roll
 Slo-mo     /   Screenshots         /   Bursts      /   Videos
 Selfies    /   Hidden              /   Time-lapse  /   Recently Added
 */
- (PHFetchResult<PHAssetCollection *> *)getSmartAlbums
{
    PHFetchResult *collections = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    return collections;
}

/*
 사진첩의 Momonts. ( 사진 저장된 순간의 묶음 이라고 보면 됨. )
 */
- (PHFetchResult<PHAssetCollection *> *)getMoments
{
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    
    PHFetchResult *collections = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeMoment subtype:PHAssetCollectionSubtypeAlbumRegular options:options];
    return collections;
}


#pragma mark -
/*
 사진첩 중에서 'albumTitle' 앨범을 리턴해준다.
 */
- (PHAssetCollection *)getAlbumWithTitle:(NSString *)albumTitle
{
    PHFetchResult *collections = [self getAlbums];
    for (PHAssetCollection *collection in collections) {
        if ([collection.localizedTitle isEqualToString:albumTitle]) {
            return collection;
        }
    }
    
    NSLog(@"----------- [%@]앨범이 없습니다. ----------- ",albumTitle);
    return nil;
}

/*
 앨범 하위의 사진을 리턴해준다.
 */
- (PHFetchResult<PHAsset *> *)getPhotoAssetsByAlbumTitle:(NSString *)albumTitle
{
//    PHFetchOptions *options = [[PHFetchOptions alloc] init];
//    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
    
    if ([self getAlbumWithTitle:albumTitle]) {
        PHFetchResult *assetsFetchResult = [PHAsset fetchAssetsInAssetCollection:[self getAlbumWithTitle:albumTitle] options:nil];
        return assetsFetchResult;
    }
    return nil;
}

- (PHFetchResult<PHAsset *> *)getPhotoAssetsByCollection:(PHAssetCollection *)collection
{
//    PHFetchOptions *options = [[PHFetchOptions alloc] init];
//    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    
    PHFetchResult *assetsFetchResult = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
    return assetsFetchResult;
}

/*
 앨범의 존재여부 확인.
 */
- (BOOL)isCreatedAlbum:(NSString *)albumTitle
{
    PHFetchResult *collections = [self getAlbums];
    for (PHAssetCollection *collection in collections) {
        if ([collection.localizedTitle isEqualToString:albumTitle]) {
            return YES;
        }
    }
    return NO;
}

- (void)createAlbumTitle:(NSString *)albumTitle block:(void(^)(BOOL isSuccess))block
{
    __block PHObjectPlaceholder *collectionPlaceholder;
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetCollectionChangeRequest *changeRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:albumTitle];
        collectionPlaceholder = changeRequest.placeholderForCreatedAssetCollection;
    } completionHandler:^(BOOL success, NSError *error) {
        block(success);
    }];
}

/*
 image 저장.
 */
- (void)saveImage:(UIImage *)image toAlbum:(NSString *)albumTitle
{
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:image];
        PHAssetCollection *collection = [self getAlbumWithTitle:albumTitle];
        PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection];
        [assetCollectionChangeRequest addAssets:@[[assetChangeRequest placeholderForCreatedAsset]]];
    } completionHandler:^(BOOL success, NSError *error) {
        if (!success) {
            NSLog(@"Error creating asset: %@", error);
        }
    }];
}

- (void)saveImages:(NSArray<UIImage *> *)images toAlbum:(NSString *)albumTitle
{
    PHAssetCollection *collection = [self getAlbumWithTitle:albumTitle];
    
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        
        NSMutableArray *assets = [NSMutableArray array];
        for (UIImage *img in images) {
            PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:img];
            [assets addObject:[assetChangeRequest placeholderForCreatedAsset]];
        }
        
        PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection];
        [assetCollectionChangeRequest addAssets:assets];
        
    } completionHandler:^(BOOL success, NSError *error) {
        if (!success) {
            NSLog(@"Error creating asset: %@", error);
        }
    }];
    
}

/*
 이미지 유지
 앨범은 삭제
 */
- (void)deleteAlbum:(NSString *)albumTitle
{
    PHAssetCollection *album = [self getAlbumWithTitle:albumTitle];
    if (album) {
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            [PHAssetCollectionChangeRequest deleteAssetCollections:@[album]];
        } completionHandler:^(BOOL success, NSError * _Nullable error) {
            if (!success) {
                NSLog(@"Error deleting asset: %@", error);
            }
        }];
    }
}

/*
 이미지 삭제 ( 카메라롤에서도 삭제 )
 앨범은 유지
 */
- (void)deleteAllImagesInAlbum:(NSString *)albumTitle
{
    PHFetchResult *assets = [[PhotoManager sharedInstance] getPhotoAssetsByAlbumTitle:albumTitle];
    if (assets) {
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            [PHAssetChangeRequest deleteAssets:assets];
        } completionHandler:^(BOOL success, NSError * _Nullable error) {
            if (!success) {
                NSLog(@"Error deleting asset: %@", error);
            }
        }];
    }
}

/*
 이미지를 앨범에서만 삭제
 카메라롤에는 유지
 */
- (void)removeAsset:(PHAsset *)asset fromAlbum:(NSString *)albumTitle
{
    if (asset) {
        PHAssetCollection *collection = [self getAlbumWithTitle:albumTitle];
        if (collection) {
            [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                PHAssetCollectionChangeRequest *changeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection];
                [changeRequest removeAssets:@[asset]];
            } completionHandler:^(BOOL success, NSError * _Nullable error) {
                if (!success) {
                    NSLog(@"Error removing asset[1]: %@", error);
                }
            }];
        }
    }
}

- (void)removeAssetIndex:(NSInteger)index fromAlbum:(NSString *)albumTitle
{
    PHAssetCollection *collection = [self getAlbumWithTitle:albumTitle];
    if (collection) {
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetCollectionChangeRequest *changeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection];
            [changeRequest removeAssetsAtIndexes:[NSIndexSet indexSetWithIndex:index]];
        } completionHandler:^(BOOL success, NSError * _Nullable error) {
            if (!success) {
                NSLog(@"Error removing asset[2]: %@", error);
            }
        }];
    }
}

/*
 앨범에서의 이미지 순서 변경
 */
- (void)moveAssetAtIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex album:(NSString *)albumTitle
{
    PHAssetCollection *collection = [self getAlbumWithTitle:albumTitle];
    if (collection) {
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetCollectionChangeRequest *changeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection];
            [changeRequest moveAssetsAtIndexes:[NSIndexSet indexSetWithIndex:fromIndex] toIndex:toIndex];
        } completionHandler:^(BOOL success, NSError * _Nullable error) {
            if (!success) {
                NSLog(@"Error moving asset: %@", error);
            }
        }];
    }
}

- (void)getImageFromAsset:(PHAsset *)asset block:(void(^)(UIImage *image))block
{
    if (!_imageManager) {
        _imageManager = [PHCachingImageManager new];
    }
    [_imageManager requestImageDataForAsset:asset options:nil resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
        if (imageData) {
            UIImage *image = [UIImage imageWithData:imageData];
            if(image && block) { block(image); }
        }
    }];
}

- (void)getImageArrayFromFetchResult:(PHFetchResult<PHAsset *> *)result block:(void(^)(NSArray *images))block
{
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:result.count];
    if(result.count > 20){
        // 최대치를 20개로 제한한다.
        for (int idx = 0; idx < 20; idx++) {
            PHAsset *asset = result[idx];
            [self getImageFromAsset:asset block:^(UIImage *image) {
                [images addObject:image];
                if(images.count == 20) {
                    block(images);
                }
            }];
        }
    }else{
        for (PHAsset *asset in result) {
            [self getImageFromAsset:asset block:^(UIImage *image) {
                [images addObject:image];
                if(images.count == result.count) {
                    block(images);
                }
            }];
        }
    }
}
@end
