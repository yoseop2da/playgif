//
//  DetailViewController.h
//  BurstGifMaker
//
//  Created by parkyoseop on 2016. 5. 26..
//  Copyright © 2016년 Yoseop Park. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
@property (strong) NSArray *imageArray;
@property (assign) CGRect lastRect;
@end
