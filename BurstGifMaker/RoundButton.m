//
//  roundButton.m
//  BurstGifMaker
//
//  Created by parkyoseop on 2016. 5. 27..
//  Copyright © 2016년 Yoseop Park. All rights reserved.
//

#import "RoundButton.h"

@implementation RoundButton

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self.layer.cornerRadius = 10.f;
        self.layer.borderColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:200/255.f alpha:1.f].CGColor;
        self.layer.borderWidth = 1.f;
        self.clipsToBounds = YES;
//        self.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1.f];
    }
    return self;
    
}
@end
