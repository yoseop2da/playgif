// #import <AssetsLibrary/AssetsLibrary.h>
//    ALAssetsLibrary *library = [ALAssetsLibrary new];
//    [library writeImageDataToSavedPhotosAlbum:data metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
//        NSLog(@"assetURL: %@",assetURL);
//        NSLog(@"Success at %@", [assetURL path] );
//    }];
//
//  DetailViewController.m
//  BurstGifMaker
//
//  Created by parkyoseop on 2016. 5. 26..
//  Copyright © 2016년 Yoseop Park. All rights reserved.
//

#import "DetailViewController.h"
#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

// 메세지, 메일
#import <MessageUI/MessageUI.h>
#import "RoundButton.h"

@interface DetailViewController ()<MFMessageComposeViewControllerDelegate>
{
    CGRect _originImageViewRect;
    float _animationTime;
    
    NSTimer *_longTapTimer;

}
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet RoundButton *speedDownButton;
@property (weak, nonatomic) IBOutlet RoundButton *speedUpButton;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _originImageViewRect = self.imageView.frame;
    
    [self addGesture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.imageView.image = _imageArray.firstObject;
    _animationTime = 0.7f;
    [self startAnimationWithDuration:_animationTime];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    [self.view layoutIfNeeded];
    self.imageView.frame = _lastRect;
}

- (void)viewDidLayoutSubviews
{
    CGFloat device_width = [UIScreen mainScreen].bounds.size.width;
    CGFloat device_height = [UIScreen mainScreen].bounds.size.height;
    [super viewDidLayoutSubviews];
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
        self.imageView.frame = CGRectMake(device_width/2.f - _originImageViewRect.size.width/2.f, device_height/2.f - _originImageViewRect.size.height/2.f, _originImageViewRect.size.width, _originImageViewRect.size.height);
    }];
}
#pragma mark - Animation
- (void)startAnimationWithDuration:(CGFloat)duration
{
    self.imageView.animationImages = _imageArray;
    self.imageView.animationDuration = duration;
    self.imageView.animationRepeatCount = 0;
    [self.imageView startAnimating];
}
#pragma mark - Button Actions

- (IBAction)backButtonTouched:(id)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
        self.imageView.frame = _lastRect;
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    }completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

- (IBAction)shareButtonTouched:(id)sender
{
    
//    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
//        [self createVideo:_imageArray videoHeight:400 block:^(NSString *savedPath) {
//            NSData *data = [[NSData alloc] initWithContentsOfFile:savedPath];
//            dispatch_async(dispatch_get_main_queue(), ^(void){
//                NSString *shareString = @"From GIF Maker.";
//                MFMessageComposeViewController *controller = [MFMessageComposeViewController new];
//                
//                if([MFMessageComposeViewController canSendText]){
//                    controller.messageComposeDelegate = self;
//                    [controller setBody:shareString];
//                    [controller addAttachmentData:data typeIdentifier:@"public.data" filename:@"gifMaker.mov"];
//                    [self presentViewController:controller animated:YES completion:nil];
//                }
//            });
//        }];
//    });
//    return;

    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSMutableArray *tempArray = [NSMutableArray array];
        CGFloat height = 600;
        if (_imageArray.count > 15) {
            height = 450;
        }else if ((_imageArray.count >= 15) && (_imageArray.count < 20)) {
            height = 350;
        }else {
            height = 300;
        }
        for (UIImage *image in [_imageArray mutableCopy]) {
            [tempArray addObject:[self resizeImage:image newHeight:height]];
        }
        
        NSString *savedGifPath = [self makeAnimatedGif:tempArray];
        NSData *data = [NSData dataWithContentsOfFile:savedGifPath];
//        dispatch_async(dispatch_get_main_queue(), ^(void){
//            NSString *shareString = @"From GIF Maker.";
//            //    UIImage *screenShotImage;// = [self capturedImage:self.view];
//            UIActivityViewController *activityViewController =
//            [[UIActivityViewController alloc] initWithActivityItems:@[shareString,data] applicationActivities:@[]];
//            
//            activityViewController.excludedActivityTypes = @[UIActivityTypePrint,UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,UIActivityTypePostToFacebook,UIActivityTypePostToTwitter];
//            [self presentViewController:activityViewController animated:YES completion:nil];
//        });
        
        NSString *shareString = @"From PlayGIF \n https://itunes.apple.com/app/id1118833818";
        MFMessageComposeViewController *controller = [MFMessageComposeViewController new];
        if([MFMessageComposeViewController canSendText]){
            controller.messageComposeDelegate = self;
            [controller setBody:shareString];
            [controller addAttachmentData:data typeIdentifier:(NSString *)kUTTypeGIF filename:@"gifMaker.gif"];
            [self presentViewController:controller animated:YES completion:nil];
        }
    });
}

- (IBAction)reverseButtonTouched:(id)sender
{
    if([self.imageView isAnimating]){
        [self.imageView stopAnimating];
    }
    _imageArray = [[_imageArray reverseObjectEnumerator] allObjects];
    [self startAnimationWithDuration:_animationTime];
}

#define MAXIMUM_VALUE 0.262500
- (IBAction)speedDownButtonTouched:(id)sender
{
    _animationTime = _animationTime + 0.05f;
    
    if (_animationTime/8.f > MAXIMUM_VALUE) {
        _animationTime = _animationTime - 0.05f;
        [self startAnimationWithDuration:_animationTime];
        return;
    }
    [self startAnimationWithDuration:_animationTime];
}

#define MINIMUM_VALUE 0.018750
- (IBAction)speedupButtonTouched:(id)sender
{
    _animationTime = _animationTime - 0.05f;
    if (_animationTime/8.f < MINIMUM_VALUE) {
        _animationTime = _animationTime + 0.05f;
        [self startAnimationWithDuration:_animationTime];
        return;
    }
    
    [self startAnimationWithDuration:_animationTime];
}

#pragma mark - GIF convert

- (NSString *)makeAnimatedGif:(NSArray *)imageArray
{
    float duration = _animationTime/8.f;
    
    NSError *error = nil;
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"animated.gif"];
    
    CGImageDestinationRef destination = CGImageDestinationCreateWithURL((CFURLRef)[NSURL fileURLWithPath:path], kUTTypeGIF, imageArray.count, NULL);
    NSDictionary *frameProperties = @{(NSString *)kCGImagePropertyGIFDictionary: @{(NSString *)kCGImagePropertyGIFDelayTime : [NSNumber numberWithFloat:duration]}};
    NSDictionary *gifProperties = @{(NSString *)kCGImagePropertyGIFDictionary: @{(NSString *)kCGImagePropertyGIFLoopCount: @(0)}};
    
    for (UIImage *image in imageArray) {
        CGImageDestinationAddImage(destination, image.CGImage, (CFDictionaryRef)frameProperties);
    }
    CGImageDestinationSetProperties(destination, (CFDictionaryRef)gifProperties);
    CGImageDestinationFinalize(destination);
    CFRelease(destination);
    
    return path;
}

- (UIImage *)resizeImage:(UIImage*)image newHeight:(CGFloat)newHeight
{
    CGFloat scale = newHeight / image.size.height;
    CGFloat newWidth = image.size.width * scale;
    CGSize size = CGSizeMake(newWidth, newHeight);
    return [self resizeImage:image size:size];
}

- (UIImage *)resizeImage:(UIImage *)image size:(CGSize )size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0,0,size.width ,size.height)];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark - MP4 convert

- (void)createVideo:(NSArray *)passedArray videoHeight:(CGFloat)newHeight block:(void(^)(NSString *savedPath))block
{
    UIImage *firstImage = passedArray.firstObject;
    CGFloat scale = newHeight / firstImage.size.height;
    CGFloat newWidth = firstImage.size.width * scale;
    CGSize imageSize = CGSizeMake(newWidth, newHeight);
    
    NSError *error = nil;
    
    NSString *videoOutputPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"output.mp4"];

    NSUInteger fps = 10;
    AVAssetWriter *videoWriter = [[AVAssetWriter alloc] initWithURL: [NSURL fileURLWithPath:videoOutputPath] fileType:AVFileTypeQuickTimeMovie error:&error];
    NSParameterAssert(videoWriter);
    NSDictionary *videoSettings = @{AVVideoCodecKey: AVVideoCodecH264,
                                    AVVideoWidthKey: [NSNumber numberWithInt:imageSize.width],
                                    AVVideoHeightKey: [NSNumber numberWithInt:imageSize.height]};
    
    AVAssetWriterInput* videoWriterInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:videoSettings];
    AVAssetWriterInputPixelBufferAdaptor *adaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:videoWriterInput sourcePixelBufferAttributes:nil];
    NSParameterAssert(videoWriterInput);
    NSParameterAssert([videoWriter canAddInput:videoWriterInput]);
    videoWriterInput.expectsMediaDataInRealTime = YES;
    [videoWriter addInput:videoWriterInput];
    
    //Start a session:
    [videoWriter startWriting];
    [videoWriter startSessionAtSourceTime:kCMTimeZero];
    
    CVPixelBufferRef buffer = NULL;
    int frameCount = 0;
    for(UIImage *img in passedArray)
    {
        UIImage *image = [self resizeImage:img size:imageSize];
        buffer = [self pixelBufferFromCGImage:image.CGImage];
        BOOL append_ok = NO;
        int j = 0;
        while (!append_ok && j < 5) {
            if (adaptor.assetWriterInput.readyForMoreMediaData)  {
                CMTime frameTime = CMTimeMake(frameCount,(int32_t) fps);
                append_ok = [adaptor appendPixelBuffer:buffer withPresentationTime:frameTime];
//                CMTimeRangeFromTimeToTime(kCMTimeZero, kCMTimePositiveInfinity)
                if(!append_ok){
                    NSError *error = videoWriter.error;
                    if(error!=nil) {
                        NSLog(@"Unresolved error %@,%@.", error, [error userInfo]);
                    }
                }
            }
            else {
                printf("adaptor not ready %d, %d\n", frameCount, j);
                [NSThread sleepForTimeInterval:0.1];
            }
            j++;
        }
        frameCount++;
    }
    [videoWriterInput markAsFinished];
    [videoWriter finishWritingWithCompletionHandler:^{
        block(videoOutputPath);
    }];
    
}

- (CVPixelBufferRef)pixelBufferFromCGImage:(CGImageRef)image
{
    CGSize frameSize = CGSizeMake(CGImageGetWidth(image), CGImageGetHeight(image));
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey, [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey, nil];
    CVPixelBufferRef pxbuffer = NULL;
    
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault, frameSize.width, frameSize.height, kCVPixelFormatType_32BGRA, (__bridge CFDictionaryRef)options, &pxbuffer);
    NSParameterAssert(status == kCVReturnSuccess && pxbuffer != NULL);
    
    CVPixelBufferLockBaseAddress(pxbuffer, 0);
    void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
    
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pxdata, frameSize.width, frameSize.height, 8, CVPixelBufferGetBytesPerRow(pxbuffer), rgbColorSpace, (CGBitmapInfo)kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    
    CGContextDrawImage(context, CGRectMake(0, 0, CGImageGetWidth(image), CGImageGetHeight(image)), image);
    CGColorSpaceRelease(rgbColorSpace);
    CGContextRelease(context);
    
    CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
    
    return pxbuffer;
}


#pragma mark -

//- (void)shareDataByMail
//{
//    NSMutableArray *dataArray = [NSMutableArray array];
//    if (self.memoData.memoPictureLinks && self.memoData.memoPictureLinks.count > 0) {
//        for (NSString *imageFileName in self.memoData.memoPictureLinks) {
//            NSData *imageData = [[NSData alloc] initWithContentsOfFile:[Common documentFullPathFromFileName:imageFileName]];
//            [dataArray addObject:@{@"fileName": imageFileName, @"data": imageData}];
//        }
//    }
//    if (self.memoData.memoVideoLink && self.memoData.memoVideoLink.length > 0) {
//        NSData *videoData = [[NSData alloc] initWithContentsOfFile:[Common documentFullPathFromFileName:self.memoData.memoVideoLink]];
//        [dataArray addObject:@{@"fileName": self.memoData.memoVideoLink, @"data": videoData}];
//    }
//    if (self.memoData.memoAudioLink && self.memoData.memoAudioLink.length > 0) {
//        NSData *audioData = [[NSData alloc] initWithContentsOfFile:[Common documentFullPathFromFileName:self.memoData.memoAudioLink]];
//        [dataArray addObject:@{@"fileName": self.memoData.memoAudioLink, @"data": audioData}];
//    }
//    
//    MFMailComposeViewController *controller = [MFMailComposeViewController new];
//    if([MFMailComposeViewController canSendMail]){
//        controller.mailComposeDelegate = self;
//        if (self.memoData.memoTitle && self.memoData.memoTitle.length > 0) {
//            [controller setSubject:[NSString stringWithFormat:@"%@",self.memoData.memoTitle]];
//        }
//        [controller setMessageBody:[Common bodyForMemoObject:self.memoData] isHTML:NO];
//        
//        for (NSDictionary *dic in dataArray) {
//            NSString *mimeType = [Common mimeTypeForFileWithFileName:dic[@"fileName"]];
//            [controller addAttachmentData:dic[@"data"] mimeType:mimeType fileName:dic[@"fileName"]];
//        }
//        
//        [self presentViewController:controller animated:YES completion:nil];
//    }
//}


#pragma mark - Message Delegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    if (result == MessageComposeResultSent) {
        NSLog(@"Message Success");
    }else if(result == MessageComposeResultFailed){
        NSLog(@"Message Fail");
    }else{
        NSLog(@"Message Cancel");
    }
    
    if([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)])
        [self dismissViewControllerAnimated:YES completion:nil];
    else
        [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Mail Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"Email Success");
    }else if(result == MFMailComposeResultSaved){
        NSLog(@"Email Saved");
    }else if(result == MFMailComposeResultFailed){
        NSLog(@"Email Failed");
    }else{
        NSLog(@"Email Cancel");
    }
    
    if([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)])
        [self dismissViewControllerAnimated:YES completion:nil];
    else
        [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

#pragma mark - Long Tap

- (void)addGesture
{
    UILongPressGestureRecognizer *longPressUpRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressUp:)];
    [_speedUpButton addGestureRecognizer:longPressUpRecognizer];
    
    UILongPressGestureRecognizer *longPressDownRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressDown:)];
    [_speedDownButton addGestureRecognizer:longPressDownRecognizer];
}

- (void)longPressUp:(UILongPressGestureRecognizer *)longGestureRecognizer
{
    if(longGestureRecognizer.state == UIGestureRecognizerStateBegan){
        _longTapTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(speedupButtonTouched:) userInfo:nil repeats:YES];
    }
    else if(longGestureRecognizer.state == UIGestureRecognizerStateEnded){
        [_longTapTimer invalidate];
        _longTapTimer = nil;
        
    }
}

- (void)longPressDown:(UILongPressGestureRecognizer *)longGestureRecognizer
{
    if(longGestureRecognizer.state == UIGestureRecognizerStateBegan){
        _longTapTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(speedDownButtonTouched:) userInfo:nil repeats:YES];
    }
    else if(longGestureRecognizer.state == UIGestureRecognizerStateEnded){
        [_longTapTimer invalidate];
        _longTapTimer = nil;
        
    }
}
@end
