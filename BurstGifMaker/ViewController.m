//
//  ViewController.m
//  BurstGifMaker
//
//  Created by parkyoseop on 2016. 5. 26..
//  Copyright © 2016년 Yoseop Park. All rights reserved.
//

#import "ViewController.h"
#import "CollectionViewCell.h"
#import "PhotoManager.h"
#import "AppDelegate.h"
#import "DetailViewController.h"

#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>
//#import <AssetsLibrary/AssetsLibrary.h>
#import "RoundButton.h"

@import Photos;
@interface ViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, PHPhotoLibraryChangeObserver>
{
    PHFetchResult *_fetchResult;
    PHCachingImageManager *_imageManager;
    CGRect _previousPreheatRect;
    
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITextView *guideTextView;
@property (weak, nonatomic) IBOutlet RoundButton *accessButton;


@end

@implementation ViewController

static int CollectionViewCellPadding = 5;
static CGSize AssetGridThumbnailSize;

- (void)viewDidLoad {
    [super viewDidLoad];
    _imageManager = [[PHCachingImageManager alloc] init];
    [[PHPhotoLibrary sharedPhotoLibrary] registerChangeObserver:self];

    [self getPhotoPermission];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    CGFloat scale = [UIScreen mainScreen].scale;
    CGSize cellSize = ((UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout).itemSize;
    AssetGridThumbnailSize = CGSizeMake(cellSize.width * scale, cellSize.height * scale);
}

- (PHAssetCollection *)getBurstsModeAlbum
{
    PHAssetCollection *burstsModeAlbum;
    PHFetchResult<PHAssetCollection *> *allAlbums = [[PhotoManager sharedInstance] getSmartAlbums];
    for (PHAssetCollection *album in allAlbums) {
        #define BURST_ALBUM_TITLE @"Bursts"
        if ([album.localizedTitle isEqualToString:BURST_ALBUM_TITLE]) {
            burstsModeAlbum = album;
            break;
        }
    }
    return burstsModeAlbum;
}

#pragma mark - Collection View

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _fetchResult.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CollectionViewCell class]) forIndexPath:indexPath];;
    
    PHAsset *asset = _fetchResult[indexPath.row];
    cell.representedAssetIdentifier = asset.localIdentifier;
    [_imageManager requestImageForAsset:asset targetSize:AssetGridThumbnailSize contentMode:PHImageContentModeAspectFill options:nil resultHandler:^(UIImage *result, NSDictionary *info)
     {
         if ([cell.representedAssetIdentifier isEqualToString:asset.localIdentifier]) {
             cell.imageView.image = result;
         }
     }];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view layoutIfNeeded];
    CollectionViewCell *cell = (CollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    CGRect lastRect = [cell convertRect:cell.bounds toView:self.view];
    DetailViewController *vc = [((AppDelegate *)[UIApplication sharedApplication].delegate).mainStoryBoard instantiateViewControllerWithIdentifier:NSStringFromClass([DetailViewController class])];
    vc.lastRect = lastRect;
    PHAsset *asset = _fetchResult[indexPath.row];
    [self getBurstsAssetsFromAsset:asset block:^(NSArray *images) {
        NSMutableArray *temp = [NSMutableArray array];
        [temp addObject:images.firstObject];
        [temp addObjectsFromArray:images];
        [temp addObject:images.lastObject];
        
        vc.imageArray = temp;
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            dispatch_async(dispatch_get_main_queue(), ^(void){
                vc.modalPresentationStyle = UIModalPresentationCustom;
                [self presentViewController:vc animated:NO completion:nil];
            });
        });
    }];
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(CollectionViewCellPadding, CollectionViewCellPadding, CollectionViewCellPadding, CollectionViewCellPadding);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float howManyCellInRow = 2;
    float width = ( [UIScreen mainScreen].bounds.size.width - CollectionViewCellPadding*(howManyCellInRow+1) ) / howManyCellInRow;
    return CGSizeMake(width, width);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, 50);
}

- (void)getPhotoPermission
{
    self.accessButton.hidden = YES;
    
    [[PhotoManager sharedInstance] checkPermission:^(BOOL isAuthorized) {
        if (isAuthorized) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self getFetchResult];
            });
        }else{
            self.accessButton.hidden = NO;
            dispatch_async(dispatch_get_main_queue(), ^{
                _guideTextView.hidden = NO;
                NSLocale *locale = [NSLocale currentLocale];
                if ([[locale objectForKey:NSLocaleLanguageCode] isEqualToString:@"ko"]) {
                    _guideTextView.text = @"\n\n\n\n\n사진첩에 접근권한이 없습니다.\n권한을 승인해주세요.";
                    [self.accessButton setTitle:@"사진첩 접근권한 허용하기" forState:UIControlStateNormal];
                }else{
                    _guideTextView.text = @"\n\n\n\n\nWe don't have permission to access to your photo library.\nPlease allow us access to the Photo Library.";
                    [self.accessButton setTitle:@"Access Photo Library" forState:UIControlStateNormal];
                }
            });
        }
    }];
}


- (IBAction)accessButtonTouched:(id)sender
{
    NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:settingsURL];
}

- (void)getFetchResult
{
    PHAssetCollection *burstsAlbum = [self getBurstsModeAlbum];
    _fetchResult = [[PhotoManager sharedInstance] getPhotoAssetsByCollection:burstsAlbum];
    
    if (_fetchResult.count > 0) {
        _guideTextView.hidden = YES;
        [self.collectionView reloadData];
    }else{
        [self noImagesText];
    }
}

- (void)noImagesText
{
    _guideTextView.hidden = NO;
    NSLocale *locale = [NSLocale currentLocale];
    
    if ([[locale objectForKey:NSLocaleLanguageCode] isEqualToString:@"ko"]) {
        _guideTextView.text = @"\n\n\n\n\n사진 앨범에,\n'고속 연사 촬영' 사진이 없습니다.\n사진 셔터를 길게 눌러\n연사촬영을 해주세요.\n\n(카메라앱>셔트를 길게 누르기)\n\n< 지원 단말 >\n- iPhone : iPhone 5S 이상\n";
    }else{
        _guideTextView.text = @"\n\n\n\n\nYou don't appear to have any\nBurst photos in your library, go\ntake some fancy ones!\n\njust go in to the Camera app and hold\ndown the shutter release button to take a Burst.\n\nIt does require you have a pretty fancy device\n(iPhone 5S or above)\n";
    }
}
- (void)getBurstsAssetsFromAsset:(PHAsset *)burstAssets block:(void(^)(NSArray *images))block
{
    PHFetchOptions *option = [PHFetchOptions new];
    option.includeAllBurstAssets = YES;
    option.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
    
    PHFetchResult *result = [PHAsset fetchAssetsWithBurstIdentifier:burstAssets.burstIdentifier options:option];
    [[PhotoManager sharedInstance] getImageArrayFromFetchResult:result block:block];
}


#pragma mark - PHPhotoLibraryChangeObserver

- (void)photoLibraryDidChange:(PHChange *)changeInstance {
    // Check if there are changes to the assets we are showing.
    PHFetchResultChangeDetails *collectionChanges = [changeInstance changeDetailsForFetchResult:_fetchResult];
    if (collectionChanges == nil) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        _fetchResult = [collectionChanges fetchResultAfterChanges];
        UICollectionView *collectionView = self.collectionView;
        if (![collectionChanges hasIncrementalChanges] || [collectionChanges hasMoves]) {
            [collectionView reloadData];
            
        } else {
            [collectionView performBatchUpdates:^{
                NSIndexSet *removedIndexes = [collectionChanges removedIndexes];
                if ([removedIndexes count] > 0) {
                    [collectionView deleteItemsAtIndexPaths:[self indexSet:removedIndexes aapl_indexPathsFromIndexesWithSection:0]];
                }
                
                NSIndexSet *insertedIndexes = [collectionChanges insertedIndexes];
                if ([insertedIndexes count] > 0) {
                    [collectionView insertItemsAtIndexPaths:[self indexSet:insertedIndexes aapl_indexPathsFromIndexesWithSection:0]];
                }
                
                NSIndexSet *changedIndexes = [collectionChanges changedIndexes];
                if ([changedIndexes count] > 0) {
                    [collectionView reloadItemsAtIndexPaths:[self indexSet:changedIndexes aapl_indexPathsFromIndexesWithSection:0]];
                }
            } completion:^(BOOL finished) {
                if (_fetchResult.count == 0) {
                    [self noImagesText];
                }else{
                    _guideTextView.hidden = YES;
                }
            }];
        }
        
        [self resetCachedAssets];
    });
}

- (NSArray *)indexSet:(NSIndexSet *)indexSet aapl_indexPathsFromIndexesWithSection:(NSUInteger)section {
    NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:indexSet.count];
    [indexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        [indexPaths addObject:[NSIndexPath indexPathForItem:idx inSection:section]];
    }];
    return indexPaths;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Update cached assets for the new visible area.
    [self updateCachedAssets];
}

#pragma mark - Asset Caching

- (void)resetCachedAssets {
    [_imageManager stopCachingImagesForAllAssets];
    _previousPreheatRect = CGRectZero;
}

- (void)updateCachedAssets {
    BOOL isViewVisible = [self isViewLoaded] && [[self view] window] != nil;
    if (!isViewVisible) { return; }
    
    // The preheat window is twice the height of the visible rect.
    CGRect preheatRect = self.collectionView.bounds;
    preheatRect = CGRectInset(preheatRect, 0.0f, -0.5f * CGRectGetHeight(preheatRect));
    
    /*
     Check if the collection view is showing an area that is significantly
     different to the last preheated area.
     */
    CGFloat delta = ABS(CGRectGetMidY(preheatRect) - CGRectGetMidY(_previousPreheatRect));
    if (delta > CGRectGetHeight(self.collectionView.bounds) / 3.0f) {
        
        // Compute the assets to start caching and to stop caching.
        NSMutableArray *addedIndexPaths = [NSMutableArray array];
        NSMutableArray *removedIndexPaths = [NSMutableArray array];
        
        [self computeDifferenceBetweenRect:_previousPreheatRect andRect:preheatRect removedHandler:^(CGRect removedRect) {
            NSArray *indexPaths = [self collectionView:_collectionView aapl_indexPathsForElementsInRect:removedRect];
            [removedIndexPaths addObjectsFromArray:indexPaths];
        } addedHandler:^(CGRect addedRect) {
            NSArray *indexPaths = [self collectionView:_collectionView aapl_indexPathsForElementsInRect:addedRect];
            [addedIndexPaths addObjectsFromArray:indexPaths];
        }];
        
        NSArray *assetsToStartCaching = [self assetsAtIndexPaths:addedIndexPaths];
        NSArray *assetsToStopCaching = [self assetsAtIndexPaths:removedIndexPaths];
        
        // Update the assets the PHCachingImageManager is caching.
        [_imageManager startCachingImagesForAssets:assetsToStartCaching
                                            targetSize:AssetGridThumbnailSize
                                           contentMode:PHImageContentModeAspectFill
                                               options:nil];
        [_imageManager stopCachingImagesForAssets:assetsToStopCaching
                                           targetSize:AssetGridThumbnailSize
                                          contentMode:PHImageContentModeAspectFill
                                              options:nil];
        
        // Store the preheat rect to compare against in the future.
        _previousPreheatRect = preheatRect;
    }
}

- (NSArray *)collectionView:(UICollectionView *)collectionView aapl_indexPathsForElementsInRect:(CGRect)rect {
    NSArray *allLayoutAttributes = [collectionView.collectionViewLayout layoutAttributesForElementsInRect:rect];
    if (allLayoutAttributes.count == 0) { return nil; }
    NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:allLayoutAttributes.count];
    for (UICollectionViewLayoutAttributes *layoutAttributes in allLayoutAttributes) {
        NSIndexPath *indexPath = layoutAttributes.indexPath;
        [indexPaths addObject:indexPath];
    }
    return indexPaths;
}

- (void)computeDifferenceBetweenRect:(CGRect)oldRect andRect:(CGRect)newRect removedHandler:(void (^)(CGRect removedRect))removedHandler addedHandler:(void (^)(CGRect addedRect))addedHandler {
    if (CGRectIntersectsRect(newRect, oldRect)) {
        CGFloat oldMaxY = CGRectGetMaxY(oldRect);
        CGFloat oldMinY = CGRectGetMinY(oldRect);
        CGFloat newMaxY = CGRectGetMaxY(newRect);
        CGFloat newMinY = CGRectGetMinY(newRect);
        
        if (newMaxY > oldMaxY) {
            CGRect rectToAdd = CGRectMake(newRect.origin.x, oldMaxY, newRect.size.width, (newMaxY - oldMaxY));
            addedHandler(rectToAdd);
        }
        
        if (oldMinY > newMinY) {
            CGRect rectToAdd = CGRectMake(newRect.origin.x, newMinY, newRect.size.width, (oldMinY - newMinY));
            addedHandler(rectToAdd);
        }
        
        if (newMaxY < oldMaxY) {
            CGRect rectToRemove = CGRectMake(newRect.origin.x, newMaxY, newRect.size.width, (oldMaxY - newMaxY));
            removedHandler(rectToRemove);
        }
        
        if (oldMinY < newMinY) {
            CGRect rectToRemove = CGRectMake(newRect.origin.x, oldMinY, newRect.size.width, (newMinY - oldMinY));
            removedHandler(rectToRemove);
        }
    } else {
        addedHandler(newRect);
        removedHandler(oldRect);
    }
}

- (NSArray *)assetsAtIndexPaths:(NSArray *)indexPaths {
    if (indexPaths.count == 0) { return nil; }
    
    NSMutableArray *assets = [NSMutableArray arrayWithCapacity:indexPaths.count];
    for (NSIndexPath *indexPath in indexPaths) {
        PHAsset *asset = _fetchResult[indexPath.item];
        [assets addObject:asset];
    }
    
    return assets;
}


@end
