//
//  PhotoManager.h
//  ImageEditor
//
//  Created by parkyoseop on 2016. 5. 19..
//  Copyright © 2016년 Yoseop Park. All rights reserved.
//

#import <Foundation/Foundation.h>

@import Photos;

@interface PhotoManager : NSObject

+ (id)sharedInstance;

- (void)checkPermission:(void(^)(BOOL isAuthorized))block;

#pragma mark - 이미지 저장 및 삭제.

- (PHFetchResult<PHAsset *> *)getAllPhotos;

- (PHFetchResult<PHAssetCollection *> *)getAlbums;
- (PHFetchResult<PHAssetCollection *> *)getSmartAlbums;
- (PHFetchResult<PHAssetCollection *> *)getMoments;

- (PHAssetCollection *)getAlbumWithTitle:(NSString *)albumTitle;

- (PHFetchResult<PHAsset *> *)getPhotoAssetsByAlbumTitle:(NSString *)albumTitle;
- (PHFetchResult<PHAsset *> *)getPhotoAssetsByCollection:(PHAssetCollection *)collection;

- (BOOL)isCreatedAlbum:(NSString *)albumTitle;
- (void)createAlbumTitle:(NSString *)albumTitle block:(void(^)(BOOL isSuccess))block;

- (void)saveImage:(UIImage *)image toAlbum:(NSString *)albumTitle;
- (void)saveImages:(NSArray<UIImage *> *)images toAlbum:(NSString *)albumTitle;

- (void)deleteAlbum:(NSString *)albumTitle;
- (void)deleteAllImagesInAlbum:(NSString *)albumTitle;
- (void)removeAsset:(PHAsset *)asset fromAlbum:(NSString *)albumTitle;
- (void)removeAssetIndex:(NSInteger)index fromAlbum:(NSString *)albumTitle;
- (void)moveAssetAtIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex album:(NSString *)albumTitle;

- (void)getImageFromAsset:(PHAsset *)asset block:(void(^)(UIImage *image))block;
- (void)getImageArrayFromFetchResult:(PHFetchResult<PHAsset *> *)result block:(void(^)(NSArray *images))block;

@end
