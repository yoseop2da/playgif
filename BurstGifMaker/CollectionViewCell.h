//
//  CollectionViewCell.h
//  BurstGifMaker
//
//  Created by parkyoseop on 2016. 5. 26..
//  Copyright © 2016년 Yoseop Park. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;


@property (nonatomic, copy) NSString *representedAssetIdentifier;

@end
